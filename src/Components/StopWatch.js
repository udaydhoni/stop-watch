import React from 'react';
class Stopwatch extends React.Component {
  constructor(props) {
    super ()
  }
  
  timerManipulator = (param=this.props.data.timerRun)=>{
    let milliSeconds = param%100 + ''
    let seconds = Math.floor(param/1000)%60 + ''
    let minutes = Math.floor((param/1000)/60) + ''
    let hours = Math.floor(((param/1000)/60)/60) + ''
    if (seconds.length ===1) {
      seconds = '0'+seconds
    } 
    if (milliSeconds.length ===1) {
      milliSeconds = '0'+milliSeconds
    }
    if (minutes.length === 1) {
      minutes = '0'+minutes
    }
    if (hours.length ===1) {
      hours = '0'+hours
    }
    console.log(hours,minutes,seconds,milliSeconds)
    return (hours+':'+minutes+':'+seconds+':'+milliSeconds)

  }
  componentWillUnmount () {
    clearInterval(this.props.unmount)
    this.props.changer()
  }
  render () {
    return(
      <div className='Stopwatch'>
        <h1>Stopwatch</h1>
        <p>{this.timerManipulator()}</p>
        <div className='footer-buttons'>
          {!this.props.data.startActive ? <button onClick={this.props.onStart}>Start</button> : 
          <div>
            {!this.props.data.stopActive ? <button onClick={this.props.onStop}>Stop</button> : 
            <div className='resume'>
              <button onClick={this.props.onResume}>Resume</button>
              <button onClick={this.props.onReset}>Reset</button>
              </div>}
          </div>
          }
          <button onClick={this.props.onClose} className='close'>Close</button>
        </div>
      </div>
    )
  }
}
