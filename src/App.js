import React from 'react';
import './App.css';
import Stopwatch from './Components/Stopwatch';



class App extends React.Component {
  constructor(props) {
    super ()
    this.state = {
      showActive: false,
      closeActive: false,
      startActive: false,
      timerStart: 0,
      timerRun:0,
      timerResume:0,
      stopActive: false
    }
  }
  showHandler = () => {
    this.setState({
      showActive: true,
      closeActive: false,
    })
  }
  closeHandler = () => {
    this.setState({
      closeActive: true,
      showActive: false,
      stopActive: false,
      startActive:false
    })
  }
  startHandler = () => {
    this.setState({
      startActive: true,
      timerStart: Date.now()
    })
    this.timer =  setInterval(()=>{
        this.setState({
          timerRun: (Date.now()-this.state.timerStart)
        })
      },10)
  
  }
  stopHandler = ()=>{
    this.setState({
      stopActive: true,
      timerResume: this.state.timerRun
    })
    console.log(this.state)
    clearInterval(this.timer)
  }
  resumeHandler = () => {
    this.setState((prevState)=>{
      prevState['timerStart']= Date.now()
      prevState['stopActive'] = false
      return prevState
      
    }, ()=>{
      console.log(this.state)
    })
    this.timer =  setInterval(()=>{
      this.setState((prevState) => {
        prevState['timerRun'] = prevState['timerResume']+Math.floor((Date.now()-this.state.timerStart))
        return prevState
      })
    },10)
  }

  resetHandler = ()=>{
    clearInterval(this.timer)
    this.setState({
      startActive: false,
      stopActive:false,
      timerRun: 0
    })
  }
  removeTimerHandler = () => {
    this.setState({
      timerRun: 0
    })
  }
  render () {
    return (
      <div className='main-container'>
        <h1>🚀 Timers🚀 </h1>
        <div className = 'buttons'>
          {!this.state.showActive ? <button id='showActive' onClick={this.showHandler}>Show Stopwatch</button> : null}
        </div>
        <div className='component'>
          {this.state.showActive && !this.state.closeActive ? <Stopwatch data={this.state} onShow = {this.showHandler} onClose={this.closeHandler} onStart={this.startHandler} onStop={this.stopHandler} onResume= {this.resumeHandler} onReset={this.resetHandler} unmount = {this.timer} changer= {this.removeTimerHandler}></Stopwatch> : null}
      
        </div>
        </div>
      
    )
  } 
}
export default App;
